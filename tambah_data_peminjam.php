<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Peminjaman</title>
</head>
<body>
    <?php  
        include 'config.php';
        $db = new Database();
    ?>

    <h3>Tambah Data Peminjam</h3>
    <form action="simpan_data_peminjam.php" method="post">
    <table>
        <tr>
            <td>Kode peminjam</td>
            <td><input type="text" name="kode_peminjam" required></td>
            <!-- Menambahkan required di atas -->
        </tr>
        <tr>
            <td>Nama</td>
            <td><input type="text" name="nama_peminjam" required></td>
            <!-- Menambahkan required di atas -->
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <select name="jenis_kelamin">
                    <option value="P">Perempuan</option>
                    <option value="L">Laki-Laki</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td>
            <td><input type="date" name="tanggal_lahir"></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><textarea name="alamat"></textarea></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td><input type="text" name="pekerjaan"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Simpan"></td>
        </tr>
    </table>
    </form>

</body>
</html>
