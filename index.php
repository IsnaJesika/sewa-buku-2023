<!-- index.php -->

<!DOCTYPE html>
<html>
<head>
    <title>Halaman Utama</title>
</head>
<body>
    <nav>
        <a href="tambah_data_peminjam.php">Tambah Data Peminjam</a>
    </nav>

    <h1>Data Peminjam</h1>

    <?php
    include 'config.php';
    $db = new Database();
    ?>

    <table border="1">
        <tr>
            <th>No</th>
            <th>Kode Peminjam</th>
            <th>Nama Peminjam</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>Pekerjaan</th>
            <th>Action</th>
        </tr>
        <?php
        $no = 1;
        foreach ($db->tampil_data() as $x) {
            ?>
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $x['kode_peminjam'] ?></td>
                <td><?php echo $x['nama_peminjam'] ?></td>
                <td><?php echo ($x['jenis_kelamin'] == 'L') ? 'Laki-Laki' : 'Perempuan' ?></td>
                <td><?php echo date("d-m-Y", strtotime($x['tanggal_lahir'])) ?></td>
                <td><?php echo $x['alamat'] ?></td>
                <td><?php echo $x['pekerjaan'] ?></td>
                <td>
                    <a href="edit_data_peminjam.php?id=<?php echo $x['kode_peminjam'] ?>">Edit</a>
                    <a href="hapus_data_peminjam.php?id=<?php echo $x['id'] ?>" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a>
                </td>
            </tr>
        <?php } ?>
    </table>

</body>
</html>
